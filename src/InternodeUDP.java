/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static java.net.StandardSocketOptions.*;

public class InternodeUDP {
    public static final short InternodeType_Undefined = 0;
    public static final short InternodeType_ReadyToReceive = 1;
    public static final short InternodeType_Greeting = 2;
    public static final short InternodeType_NodeDataRequest = 3;
    public static final short InternodeType_NodeDataResponse = 4;

    public static class InternodeHeader {
        public short internodeType = InternodeType_Undefined;
        public short dataSize = 0;
    }

    public static class InternodeMessage {
        public InternodeHeader header;
        public ByteBuffer data;

        InetSocketAddress fromAddr;

        private final String portString;

        public InternodeMessage(InternodeHeader header, ByteBuffer data, InetSocketAddress fromAddr) {
            this.header = header;
            this.data = data;
            this.fromAddr = fromAddr;

            portString = Integer.toString(fromAddr.getPort());
        }

        public String getPortString() {
            return portString;
        }
    }

    public static class ReceivedInternode {
        public ArrayList<InternodeMessage> internodeMessages = new ArrayList<>();
    }

    private final DatagramChannel socket;

    private SelectionKey selectionKey = null;

    // Port -> Name
    HashMap<String, String> neighbourPortToName = new HashMap<>();
    // Name -> Port
    HashMap<String, String> neighbourNameToPort = new HashMap<>();
    // Port -> Unique ID
    HashMap<String, Short> neighbourPortToUniqueID = new HashMap<>();
    // Name -> Unique ID
    HashMap<String, Short> neighbourNameToUniqueID = new HashMap<>();
    // Unique ID -> Name
    HashMap<Short, String> neighbourUniqueIDToName = new HashMap<>();
    // Unique ID -> Port
    HashMap<Short, String> neighbourUniqueIDToPort = new HashMap<>();

    public static final short HEADER_SIZE = 4;
    public static final short MAX_PACKET_SIZE = 1500; // Should prevent any fragmentation, and from my understanding unsure all UDP packets are sent in one call to 'sendto'
    public static final short MAX_MESSAGE_SIZE = MAX_PACKET_SIZE - HEADER_SIZE;

    public InternodeUDP(String name, short uniqueID, String port, ArrayList<String> neighbourPorts) throws IOException {

        socket = DatagramChannel.open();
        socket.setOption(SO_REUSEADDR, true);
        socket.setOption(SO_REUSEPORT, true);
        socket.setOption(SO_RCVBUF, MAX_PACKET_SIZE * 100 * 100);
        socket.setOption(SO_SNDBUF, MAX_PACKET_SIZE * 100 * 100);
        socket.bind(new InetSocketAddress((InetAddress) null, Integer.parseInt(port)));
        socket.configureBlocking(false); // Needed for select

        syncWithNeighbours(name, uniqueID, neighbourPorts);
    }

    private void syncWithNeighbours(String name, short uniqueID, ArrayList<String> neighbourPorts) throws IOException {
        int expected = neighbourPorts.size();

        // Set of ports greeting message has been sent to
        HashSet<String> sentGreetings = new HashSet<>();

        Selector selector = Selector.open();
        registerForSelect(selector);

        do {
            // Send RTR to all neighbours, yet to be heard back from
            for (String neighbourPort : neighbourPorts) {
                if (!neighbourPortToName.containsKey(neighbourPort)) {
                    sendMessage(neighbourPort, InternodeType_ReadyToReceive, ByteBuffer.allocate(0));
                }
            }

            // Wait for a message, or time out

            selector.select(10);

            // While there are immediately some messages to receive, process and if needed respond to them
            while (true) {
                ReceivedInternode received = processSelect();

                for (InternodeMessage message : received.internodeMessages) {
                    switch (message.header.internodeType) {
                        case InternodeType_ReadyToReceive: {

                            ByteBuffer data = ByteBuffer.allocate(2 + name.length());
                            data.order(ByteOrder.nativeOrder()); // Since unique ID always in network order
                            data.putShort(uniqueID);
                            data.order(ByteOrder.BIG_ENDIAN); // Back to network
                            data.put(name.getBytes(StandardCharsets.UTF_8));
                            data.flip();

                            sendMessage(message, InternodeType_Greeting, data);
                            sentGreetings.add(message.getPortString());
                            break;
                        }
                        case InternodeType_Greeting: {

                            message.data.order(ByteOrder.nativeOrder());  // Since unique ID always in network order
                            short neighbourUniqueID = message.data.getShort();
                            message.data.order(ByteOrder.BIG_ENDIAN); // Back to network

                            byte[] neighbourNameBuf = new byte[message.data.remaining()];
                            message.data.get(neighbourNameBuf);

                            String neighbourName = new String(neighbourNameBuf, StandardCharsets.UTF_8);
                            String neighbourPort = message.getPortString();


                            neighbourPortToUniqueID.put(neighbourPort, neighbourUniqueID);
                            neighbourNameToUniqueID.put(neighbourName, neighbourUniqueID);
                            neighbourPortToName.put(neighbourPort, neighbourName);
                            neighbourNameToPort.put(neighbourName, neighbourPort);
                            neighbourUniqueIDToName.put(neighbourUniqueID, neighbourName);
                            neighbourUniqueIDToPort.put(neighbourUniqueID, neighbourPort);

                            break;
                        }
                        default: {
                            System.err.println("Got unexpected message type while syncing with neighbours: " + message.header.internodeType);
                        }
                    }
                }

                int ready = selector.select(1);
                if (ready == 0) {
                    break;
                }
            }

            // Once we have told all out neighbours our name, and their ours we are synced
        } while (sentGreetings.size() != expected || neighbourNameToPort.size() != expected);

        selector.close();

        System.out.println(name + " is now Synced");
    }

    private void sendMessage(InetSocketAddress addr, short type, ByteBuffer data) throws IOException {
        short dataSize = (short) data.limit();

        if (dataSize > MAX_MESSAGE_SIZE) {
            throw new IllegalArgumentException("Messages have a max data size of " + MAX_MESSAGE_SIZE + " bytes, tried: " + dataSize + " bytes, of type: " + type);
        }

        ByteBuffer submitBuffer = ByteBuffer.allocate(HEADER_SIZE + dataSize);

        submitBuffer.putShort(type);
        submitBuffer.putShort(dataSize);
        submitBuffer.put(data);
        submitBuffer.flip();

        int sent = socket.send(submitBuffer, addr);
        if (sent != submitBuffer.limit()) {
            throw new RuntimeException("Expected to send entire pack over UDP in send one call");
        }

        // From my understanding, when using blocking send over UDP, it will either send the entire packet or block, never partial
        // So long as the sndbuf is big enough, which is should most likely be
    }

    public void sendMessage(String port, short type, ByteBuffer data) throws IOException {
        sendMessage(new InetSocketAddress((InetAddress) null, Integer.parseInt(port)), type, data);
    }

    public void sendMessage(InternodeMessage respondTo, short type, ByteBuffer data) throws IOException {
        sendMessage(respondTo.fromAddr, type, data);
    }

    public void registerForSelect(Selector selector) throws ClosedChannelException {
        if (selectionKey != null) {
            selectionKey.cancel();
        }
        selectionKey = socket.register(selector, SelectionKey.OP_READ);
    }

    public ReceivedInternode processSelect() throws IOException {
        if (!selectionKey.isReadable()) {
            return new ReceivedInternode();
        }

        ReceivedInternode receivedInternode = new ReceivedInternode();

        ByteBuffer writeBuffer = ByteBuffer.allocate(MAX_PACKET_SIZE);

        Selector selector = Selector.open();
        SelectionKey tempSelectionKey = socket.register(selector, SelectionKey.OP_READ);

        while (true) {
            InetSocketAddress addr = (InetSocketAddress) socket.receive(writeBuffer);
            if (addr == null) {
                break;
            }

            writeBuffer.flip();

            InternodeHeader header = new InternodeHeader();
            header.internodeType = writeBuffer.getShort();
            header.dataSize = writeBuffer.getShort();

            if (header.dataSize != writeBuffer.remaining()) {
                throw new RuntimeException("Should always receive entire message in one call to recvfrom, perhaps invalid data send? | " + header.dataSize + " != " + (writeBuffer.remaining()));
            }

            ByteBuffer dataBuffer = ByteBuffer.allocate(writeBuffer.remaining());
            dataBuffer.put(writeBuffer);
            dataBuffer.flip();

            receivedInternode.internodeMessages.add(new InternodeMessage(
                    header,
                    dataBuffer,
                    addr
            ));

            break;

            /// NOTE: For some reason, I can't collect multiple messages at the same time? This is exactly
            /// what I am doing in c++, yet here for some reason, somehow it's changing the the addr I store
            /// from previous call, and even when I immediately extract the port as a string it still does it,
            /// at this point I give up on trying to find why, this is why Rust was invented.

            /*int avail = selector.select(1);
            if (avail == 0) {
                break;
            }*/
        }

        tempSelectionKey.cancel();
        selector.close();

        return receivedInternode;
    }

    boolean hasPortWiseNeighbour(String name) {
        return neighbourNameToPort.containsKey(name);
    }


    String getNeighbourPort(String name) {
        return neighbourNameToPort.get(name);
    }


    String getNeighbourName(String port) {
        return neighbourPortToName.get(port);
    }

    short getNeighbourUniqueIDFromPort(String port) {
        return neighbourPortToUniqueID.get(port);
    }

    short getNeighbourUniqueIDFromName(String name) {
        return neighbourNameToUniqueID.get(name);
    }


    String getNeighbourNameFromUID(short uniqueID) {
        return neighbourUniqueIDToName.get(uniqueID);
    }


    String getNeighbourPortFromUID(short uniqueID) {
        return neighbourUniqueIDToPort.get(uniqueID);
    }
}
