/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class ShortestPathTraversal {

    // Byte representation:                 | Byte offsets:
    // traversal_id: 8 bytes                | 0
    // depart_time: 2 bytes                 | 8 bytes
    // path_len: 2 bytes                    | 10 bytes
    // path_index: 2 bytes                  | 12 bytes
    // path: (path_len) * 2 bytes           | 14 bytes
    // dest_name_len: 2 bytes               | 14 + ((path_len) * 2) bytes
    // dest_name: (dest_name_len) bytes     | 16 + ((path_len) * 2) bytes
    public static class NodeDataRequest {
        public long traversalID;
        public String destinationName;
        public ArrayList<Short> path;
        public short currentPathIndex;
        public short departTime;

        public NodeDataRequest(long traversalID, String destinationName, ArrayList<Short> path, short currentPathIndex, short departTime) {
            this.traversalID = traversalID;
            this.destinationName = destinationName;
            this.path = path;
            this.currentPathIndex = currentPathIndex;
            this.departTime = departTime;
        }

        public static NodeDataRequest from_data(ByteBuffer data) {

            long traversalID = data.getLong();
            short departTime = data.getShort();
            short pathLen = data.getShort();
            short currentPathIndex = data.getShort();

            ArrayList<Short> path = new ArrayList<>();

            data.order(ByteOrder.nativeOrder()); // Gets path uid's in as it is in memory, which will already be network endian, to conform with c++ impl
            for (int i = 0; i < pathLen; i++) {
                path.add(data.getShort());
            }
            data.order(ByteOrder.BIG_ENDIAN); // Revert to network endian

            short destNameLen = data.getShort();

            byte[] name_buf = new byte[destNameLen];
            data.get(name_buf);

            String destinationName = new String(name_buf, StandardCharsets.UTF_8);

            if (data.position() != data.capacity()) {
                throw new RuntimeException("Data should be expected size");
            }

            return new NodeDataRequest(
                    traversalID,
                    destinationName,
                    path,
                    currentPathIndex,
                    departTime
            );
        }

        public ByteBuffer toData() {
            int expectedSize = (8 + 2 + 2 + 2 + (2 * path.size()) + 2 + destinationName.length());

            ByteBuffer data = ByteBuffer.allocate(expectedSize); // Defaults to big endian == network endian
            data.putLong(traversalID);
            data.putShort(departTime);
            data.putShort((short) path.size());
            data.putShort(currentPathIndex);

            data.order(ByteOrder.nativeOrder()); // Puts path uid's in as it is in memory, which will already be network endian, to conform with c++ impl
            for (Short pathUID : path) {
                data.putShort(pathUID);
            }
            data.order(ByteOrder.BIG_ENDIAN); // Revert to network endian

            data.putShort((short) destinationName.length());
            data.put(destinationName.getBytes(StandardCharsets.UTF_8));

            if (data.position() != expectedSize) {
                throw new RuntimeException("Data should be expected size");
            }

            data.flip();

            return data;
        }
    }

    // Byte representation:                                 | Byte offsets:
    // traversal_id: 8 bytes                                | 0 bytes
    // path_len: 2 bytes                                    | 8 bytes
    // path_index: 2 bytes                                  | 10 bytes
    // path: (path_len) * 2 bytes                           | 12 bytes
    // neighbour_len: 2 bytes                               | 12 + ((path_len) * 2) bytes
    // neighbour_is_destination_index: 2 bytes              | 14 + ((path_len) * 2) bytes
    // neighbour_uids: (neighbour_len) * 2 bytes            | 16 + ((path_len) * 2) bytes
    // neighbour_arrival_times: (neighbour_len) * 2 bytes   | 16 + ((path_len) * 2) + ((neighbour_len) * 2) bytes
    public static class NodeDataResponse {
        public long traversalID;
        public ArrayList<Short> path = new ArrayList<>();
        public ArrayList<Short> uniqueIDs = new ArrayList<>();
        public ArrayList<Short> arrivalTimes = new ArrayList<>();
        public short currentPathIndex = 0;
        public short isDestIndex = -1;

        public NodeDataResponse() {
        }

        public NodeDataResponse(long traversalID, ArrayList<Short> path, ArrayList<Short> uniqueIDs, ArrayList<Short> arrivalTimes, short currentPathIndex, short isDestIndex) {
            this.traversalID = traversalID;
            this.path = path;
            this.uniqueIDs = uniqueIDs;
            this.arrivalTimes = arrivalTimes;
            this.currentPathIndex = currentPathIndex;
            this.isDestIndex = isDestIndex;
        }

        static NodeDataResponse fromData(ByteBuffer data) {

            long traversalID = data.getLong();
            short pathLen = data.getShort();
            short currentPathIndex = data.getShort();

            ArrayList<Short> path = new ArrayList<>();
            data.order(ByteOrder.nativeOrder()); // Gets path uid's in as it is in memory, which will already be network endian, to conform with c++ impl
            for (int i = 0; i < pathLen; i++) {
                path.add(data.getShort());
            }
            data.order(ByteOrder.BIG_ENDIAN); // Revert to network endian

            short neighbourLen = data.getShort();
            short isDestIndex = data.getShort();

            ArrayList<Short> uniqueIDs = new ArrayList<>();
            data.order(ByteOrder.nativeOrder()); // Gets neighbour uid's in as it is in memory, which will already be network endian, to conform with c++ impl
            for (int i = 0; i < neighbourLen; i++) {
                uniqueIDs.add(data.getShort());
            }
            data.order(ByteOrder.BIG_ENDIAN); // Revert to network endian

            ArrayList<Short> arrivalTimes = new ArrayList<>();
            for (int i = 0; i < neighbourLen; i++) { // Need to convert from network as used for numeric
                arrivalTimes.add(data.getShort());
            }

            return new NodeDataResponse(
                    traversalID,
                    path,
                    uniqueIDs,
                    arrivalTimes,
                    currentPathIndex,
                    isDestIndex

            );
        }

        public ByteBuffer toData() {
            if (arrivalTimes.size() != uniqueIDs.size()) {
                throw new RuntimeException("Number of arrival times must equal number of unique ids");
            }

            int expectedSize = 8 + 2 + 2 + (2 * path.size()) + 2 + 2 + (2 * uniqueIDs.size()) + (2 * arrivalTimes.size());

            ByteBuffer data = ByteBuffer.allocate(expectedSize); // Defaults to big endian == network endian

            data.putLong(traversalID);
            data.putShort((short) path.size());
            data.putShort(currentPathIndex);

            data.order(ByteOrder.nativeOrder()); // Puts path uid's in as it is in memory, which will already be network endian, to conform with c++ impl
            for (Short pathUID : path) {
                data.putShort(pathUID);
            }
            data.order(ByteOrder.BIG_ENDIAN); // Revert to network endian

            data.putShort((short) uniqueIDs.size());
            data.putShort(isDestIndex);

            data.order(ByteOrder.nativeOrder()); // Puts neighbour uid's in as it is in memory, which will already be network endian, to conform with c++ impl
            for (Short neighbourUID : uniqueIDs) {
                data.putShort(neighbourUID);
            }
            data.order(ByteOrder.BIG_ENDIAN); // Revert to network endian

            for (Short arrivalTime : arrivalTimes) { // Need to convert to network as used for numeric
                data.putShort(arrivalTime);
            }

            if (data.position() != expectedSize) {
                throw new RuntimeException("Data should be expected size");
            }

            data.flip();

            return data;
        }
    }

    public static class TravelNode {
        // Path of unique ID's to take to reach node
        public ArrayList<Short> path;
        public short arrivalTime;
        public boolean isDestination;
        public short dayJumps;

        public TravelNode(ArrayList<Short> path, short arrivalTime, boolean isDestination, short day_jumps) {
            this.path = path;
            this.arrivalTime = arrivalTime;
            this.isDestination = isDestination;
            this.dayJumps = day_jumps;
        }
    }

    private static long nextSubID = 1;

    private final long traversalID;

    // Travel time, Unique ID
    private final UniqueLowPriorityQueue<Short, Short> frontier = new UniqueLowPriorityQueue<>();
    // Unique ID of those visited
    private final HashSet<Short> visited = new HashSet<>();
    // Unique ID -> Travel Node
    private final HashMap<Short, TravelNode> nodes = new HashMap<>();

    private final short leaveTime;

    private final String destinationName;

    private Short destinationNode = null;

    private boolean waitingOnResponse = false;
    private boolean noMoreRequests = false;

    private Short lastRequestNodeUID = 0;
    private Short lastRequestPathTravelTime = 0;

    public ShortestPathTraversal(short uniqueID, String name, String destinationName, short leaveTime, InternodeUDP internodeUDP, Timetable timetable) {
        this.leaveTime = leaveTime;
        this.destinationName = destinationName;

        traversalID = (((long) uniqueID) << 48) + nextSubID;
        nextSubID++;

        TravelNode node = new TravelNode(
                new ArrayList<>(Collections.singletonList(uniqueID)),
                (short) 0,
                destinationName.equals(name),
                (short) 0
        );

        visited.add(uniqueID);
        nodes.put(uniqueID, node);

        if (destinationName.equals(name)) {
            destinationNode = uniqueID;
            noMoreRequests = true;
            return;
        }

        for (String neighbourDestination : timetable.getDestinations()) {
            if (internodeUDP.hasPortWiseNeighbour(neighbourDestination)) {
                short neighbour_uid = internodeUDP.getNeighbourUniqueIDFromName(neighbourDestination);

                Timetable.TimetableEntry arrivalEntry = timetable.getEarliestTimetableEntryByDest(neighbourDestination, leaveTime);

                TravelNode neighbourNode = new TravelNode(
                        new ArrayList<>(Arrays.asList(uniqueID, neighbour_uid)),
                        arrivalEntry.arrivalTime,
                        destinationName.equals(neighbourDestination),
                        (short) 0
                );

                nodes.put(neighbour_uid, neighbourNode);

                short travelTime;
                if (arrivalEntry.arrivalTime > leaveTime) {
                    travelTime = (short) (arrivalEntry.arrivalTime - leaveTime);
                } else {
                    travelTime = (short) ((arrivalEntry.arrivalTime + 24 * 60) - leaveTime);
                    nodes.get(neighbour_uid).dayJumps++;
                }

                frontier.push(travelTime, neighbour_uid);
            } else {
                Timetable.TimetableEntry arrivalEntry = timetable.getEarliestTimetableEntryByDest(neighbourDestination, leaveTime);

                // use 0 for unique ID as, there is no server but I need to provide something

                TravelNode neighbourNode = new TravelNode(
                        new ArrayList<>(Arrays.asList(uniqueID, (short) 0)),
                        arrivalEntry.arrivalTime,
                        destinationName.equals(neighbourDestination),
                        (short) 0
                );

                nodes.put((short) 0, neighbourNode);

                short travelTime;
                if (arrivalEntry.arrivalTime > leaveTime) {
                    travelTime = (short) (arrivalEntry.arrivalTime - leaveTime);
                } else {
                    travelTime = (short) ((arrivalEntry.arrivalTime + 24 * 60) - leaveTime);
                    nodes.get((short) 0).dayJumps++;
                }

                frontier.push(travelTime, (short) 0);
            }
        }
    }

    public boolean isComplete() {
        return noMoreRequests;
    }

    public TravelNode result() {
        if (destinationNode != null && nodes.containsKey(destinationNode)) {
            return nodes.get(destinationNode);
        }
        return null;
    }

    short getStartTime() {
        return leaveTime;
    }

    String getDestinationName() {
        return destinationName;
    }

    long getTraversalID() {
        return traversalID;
    }

    NodeDataRequest traverse(NodeDataResponse response) {
        if (isComplete()) {
            return null;
        }

        if ((response == null) == waitingOnResponse) {
            throw new IllegalArgumentException("Shortest path got response when not expecting, or vis-versa");
        }

        if (response != null) {
            waitingOnResponse = false;

            for (int index = 0; index < response.uniqueIDs.size(); index++) {
                short uid = response.uniqueIDs.get(index);

                if (visited.contains(uid)) {
                    // Skip adding already visited
                    continue;
                }

                TravelNode lastNode = nodes.get(lastRequestNodeUID);

                // Shallow copy is fine since never mutating stored id's, only the list itself
                ArrayList<Short> path = (ArrayList<Short>) lastNode.path.clone();
                path.add(uid);

                short arrivalTime = response.arrivalTimes.get(index);

                TravelNode node = new TravelNode(
                        path,
                        arrivalTime,
                        index == response.isDestIndex,
                        lastNode.dayJumps
                );

                nodes.put(uid, node);

                // This logic accounts for looping around to the next day, but assumes that no single jump is > 24h (seems reasonable)
                short travelTime;
                if (arrivalTime >= lastNode.arrivalTime) {
                    travelTime = (short) ((arrivalTime - lastNode.arrivalTime) + lastRequestPathTravelTime);
                } else {
                    // arrival_time is the next day, so add 24h worth of time
                    nodes.get(uid).dayJumps++;
                    travelTime = (short) ((arrivalTime + (24 * 60) - lastNode.arrivalTime) + lastRequestPathTravelTime);
                }

                frontier.push(travelTime, uid);
            }

        }

        // Nothing to do if empty
        if (frontier.empty()) {
            noMoreRequests = true;
            return null;
        }

        // get top
        UniqueLowPriorityQueue<Short, Short>.PriorityValuePair node = frontier.topPair();
        frontier.pop();

        // if destination, search is complete
        if (nodes.get(node.value).isDestination) {
            destinationNode = node.value;
            waitingOnResponse = false;
            noMoreRequests = true;
            return null;
        }

        // Add to visited
        visited.add(node.value);

        // Shallow copy is fine since never mutating stored id's, only the list itself
        ArrayList<Short> path = (ArrayList<Short>) nodes.get(node.value).path.clone();
        lastRequestNodeUID = node.value;
        lastRequestPathTravelTime = node.priority;

        // else need to add it's neighbours, which are got via a request
        NodeDataRequest request = new NodeDataRequest(
                traversalID,
                destinationName,
                path,
                (short) 1, // Start path to destination by going ot the [1] in the path, since this current server is [0] in path
                nodes.get(node.value).arrivalTime
        );

        waitingOnResponse = true;
        return request;
    }
}
