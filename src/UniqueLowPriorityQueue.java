/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

import java.util.HashMap;
import java.util.HashSet;

public class UniqueLowPriorityQueue<Priority extends Comparable<Priority>, Value> {
    private final HashMap<Priority, HashSet<Value>> perPriorityValueSet = new HashMap<>();
    private final HashMap<Value, Priority> valuePriorityLookup = new HashMap<>();

    private Priority minPriority() {
        Priority min = null;

        for (Priority p : perPriorityValueSet.keySet()) {
            if (min == null || p.compareTo(min) < 0) {
                min = p;
            }
        }

        return min;
    }

    public Value topValue() {
        Priority topP = minPriority();
        return perPriorityValueSet.get(topP).iterator().next();
    }

    public Priority topPriority() {
        return minPriority();
    }

    public PriorityValuePair topPair() {
        Priority topP = minPriority();
        Value topVal = perPriorityValueSet.get(topP).iterator().next();

        return new PriorityValuePair(topP, topVal);
    }

    public boolean empty() {
        return valuePriorityLookup.isEmpty();
    }

    public int size() {
        return valuePriorityLookup.size();
    }

    public boolean push(Priority priority, Value value) {

        if (valuePriorityLookup.containsKey(value)) {
            // Value already exists

            // Get associated priority
            Priority old_p = valuePriorityLookup.get(value);

            if (old_p.compareTo(priority) < 0) {
                // Current one already has a lower priority, so don't insert
                return false;
            }

            // Remove old lookup
            valuePriorityLookup.remove(value);

            // Remove old value
            perPriorityValueSet.get(old_p).remove(value);

            // Remove priority set if empty
            if (perPriorityValueSet.get(old_p).isEmpty()) {
                perPriorityValueSet.remove(old_p);
            }
        }

        // add new lookup
        valuePriorityLookup.put(value, priority);

        // Add new value
        if (!perPriorityValueSet.containsKey(priority)) {
            perPriorityValueSet.put(priority, new HashSet<>());
        }

        perPriorityValueSet.get(priority).add(value);

        return true;

    }

    public void pop() {
        PriorityValuePair top = topPair();

        // Remove value from set
        perPriorityValueSet.get(top.priority).remove(top.value);

        // Remove set if empty
        if (perPriorityValueSet.get(top.priority).isEmpty()) {
            perPriorityValueSet.remove(top.priority);
        }

        // Remove lookup
        valuePriorityLookup.remove(top.value);
    }

    public class PriorityValuePair {
        Priority priority;
        Value value;

        public PriorityValuePair(Priority priority, Value value) {
            this.priority = priority;
            this.value = value;
        }
    }

}
