/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.Selector;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static class TraversalInstance {
        ShortestPathTraversal traversal;
        WebTCPListener.WebTCPResponder responder;
        Timetable timetable;

        public TraversalInstance(ShortestPathTraversal traversal, WebTCPListener.WebTCPResponder responder, Timetable timetable) {
            this.traversal = traversal;
            this.responder = responder;
            this.timetable = timetable;
        }
    }

    public static void main(String[] args) throws IOException {
        if (args.length < 3) {
            throw new IllegalArgumentException("Not enough parameters!");
        }

        // Load up arguments as name and ports
        String name = args[0];
        String webTCPPort = args[1];
        String internodeUDPPort = args[2];

        System.out.println("Hello, World from (Java) Station: " + name);
        System.out.println("TCP port: " + webTCPPort + " | UDP port: " + internodeUDPPort);

        ArrayList<String> neighbourUDPPorts = new ArrayList<>(args.length - 3);
        for (int i = 3; i < args.length; i++) {
            String portStr = args[i];

            try {
                Integer.parseInt(portStr);
            } catch (NumberFormatException e) {
                continue; // Not port, just neighbour name probably
            }

            neighbourUDPPorts.add(portStr);
            System.out.println("Adjacent UDP port (" + (i - 3) + "): " + portStr);
        }

        // Use web tcp port in network presentation as a unique ID, not actually used as port, just a unique ID
        // specifying one on startup would be an alternative if not all localhost and thus ports not unique
        short uniqueID = Toolbox.htons(Short.parseShort(webTCPPort));

        // Create and setup tcp and udp ports, ready to receive
        long timetableLastModified = Timetable.getLastModified(name);
        Timetable timetable = new Timetable(name);

        WebTCPListener webTCPListener = new WebTCPListener(webTCPPort);
        InternodeUDP internodeUDP = new InternodeUDP(name, uniqueID, internodeUDPPort, neighbourUDPPorts);

        for (String neighbourUDPPort : neighbourUDPPorts) {
            System.out.println("Has neighbour: " + internodeUDP.getNeighbourName(neighbourUDPPort) + " | with uid: " + internodeUDP.getNeighbourUniqueIDFromPort(neighbourUDPPort));
        }

        // Used to allow for multiple requests to the same server at once
        HashMap<Long, TraversalInstance> traversalInstanceMap = new HashMap<>();

        Selector selector = Selector.open();
        webTCPListener.registerForSelect(selector);
        internodeUDP.registerForSelect(selector);

        while (true) {
            // "Poll" for if this server needs to do anything
            selector.select();

            // Reload timetable if needed
            long newTimetableLastModified = Timetable.getLastModified(name);
            if (newTimetableLastModified > timetableLastModified) {
                timetableLastModified = newTimetableLastModified;
                timetable = new Timetable(name);
            }

            WebTCPListener.WebResponse webResponse = webTCPListener.processSelect();

            if (webResponse != null) {
                // There was a http request, so read it and prepare response
                WebTCPListener.HTTPRequest httpRequest = webResponse.httpRequest;
                WebTCPListener.WebTCPResponder responder = webResponse.webTCPResponder;

                // Get the destination from request
                String URI = httpRequest.extractGetURI();
                HashMap<String, String> getPairs = httpRequest.extractGetKeyValuePairs(URI);

                // Get if has valid "to=X" in request
                if (getPairs.containsKey("to")) {

                    String destination = getPairs.get("to");
                    System.out.println("Got query for destination: " + destination);

                    short requestTime = Toolbox.getNowTimeInt();

                    // Start traversal of network via shortest path algorithm
                    ShortestPathTraversal traversal = new ShortestPathTraversal(uniqueID, name, destination, requestTime, internodeUDP, timetable);

                    if (traversal.isComplete()) {
                        // If traversal is already complete you are at the destination

                        String response;
                        if (traversal.result() != null) {
                            response = "You are already at: " + destination;
                        } else {
                            response = "You can not reach: " + destination;
                        }

                        responder.sendAndClose("200 OK", response);

                    } else {
                        // Else, must start traversing
                        ShortestPathTraversal.NodeDataRequest request = traversal.traverse(null);

                        if (request == null) {
                            // The traversal ended, and made no request for more information so
                            // either (dest is adjacent AND that is quickest path) OR (there is no path)

                            String response;
                            if (traversal.result() != null) {
                                // Traversal has a result, so shortest path is already found

                                short firstStopUID = traversal.result().path.get(1);

                                String firstStopName = internodeUDP.getNeighbourNameFromUID(firstStopUID);

                                Timetable.TimetableEntry firstStop = timetable.getEarliestTimetableEntryByDest(firstStopName, traversal.getStartTime());

                                response = Toolbox.formResponseImmediate(firstStop, requestTime);
                            } else {
                                // No way to get to destination (aka this server has no neighbours)

                                response = "You can not reach: " + destination;
                            }

                            responder.sendAndClose("200 OK", response);
                        } else {
                            // The traversal made a request for more info, so we send that request onto it's destination, and store the traversal instance

                            ByteBuffer requestData = request.toData();

                            String requestPort = internodeUDP.getNeighbourPortFromUID(request.path.get(request.currentPathIndex));

                            internodeUDP.sendMessage(requestPort, InternodeUDP.InternodeType_NodeDataRequest, requestData);

                            TraversalInstance traversalInstance = new TraversalInstance(
                                    traversal,
                                    responder,
                                    timetable // Timetable stored for once the shortest path is found and building up a message based on that path to present to client
                            );

                            traversalInstanceMap.put(traversal.getTraversalID(), traversalInstance);
                        }
                    }

                } else {
                    responder.sendAndClose("404 Not Found", "");

                    System.err.println("IGNORING: Got request with invalid to= Get request, or possible a favicon request or something");
                }
            }

            // Check if there was udp event
            InternodeUDP.ReceivedInternode receivedInternode = internodeUDP.processSelect();
            for (InternodeUDP.InternodeMessage msg : receivedInternode.internodeMessages) {
                // If so process the message depending in type, ignoring those we don't expect to get

                switch (msg.header.internodeType) {
                    case InternodeUDP.InternodeType_Undefined:
                        //System.out.println("IGNORING Received 'Undefined' internode packet with " + msg.data.remaining() + " bytes");
                        break;

                    case InternodeUDP.InternodeType_ReadyToReceive:
                        //System.out.println(name + ": HANDLING Received 'ReadyToReceive' internode packet with " + msg.data.remaining() + " bytes");

                        /// NOTE: This really shouldn't be needed, unless there is packet loss, and for
                        /// some reason I was consistently getting packet loss when testing, but never in the
                        /// c++ version, so I hope this packet loss stops after the sync phase.

                        ByteBuffer data = ByteBuffer.allocate(2 + name.length());
                        data.order(ByteOrder.nativeOrder()); // Since unique ID always in network order
                        data.putShort(uniqueID);
                        data.order(ByteOrder.BIG_ENDIAN); // Back to network
                        data.put(name.getBytes());
                        data.flip();

                        internodeUDP.sendMessage(msg, InternodeUDP.InternodeType_Greeting, data);

                        break;

                    case InternodeUDP.InternodeType_Greeting:
                        //System.out.println("IGNORING Received 'Greeting' internode packet with " + msg.data.remaining() + " bytes");
                        break;

                    case InternodeUDP.InternodeType_NodeDataRequest: {
                        System.out.println("PROCESSING Received 'NodeDataRequest' internode packet with " + msg.data.remaining() + " bytes");
                        // A request for data about a node was received

                        ShortestPathTraversal.NodeDataRequest request = ShortestPathTraversal.NodeDataRequest.from_data(msg.data);

                        if (request.currentPathIndex == request.path.size() - 1) {
                            // Message meant for this server, send back response

                            // Build response
                            ShortestPathTraversal.NodeDataResponse response = new ShortestPathTraversal.NodeDataResponse();
                            response.traversalID = request.traversalID;
                            response.path = request.path;

                            response.isDestIndex = -1;
                            for (String destinationName : timetable.getDestinations()) {
                                // Go through all destinations immediately reachable

                                if (internodeUDP.hasPortWiseNeighbour(destinationName)) {
                                    // If that destination has a port, aka there is a server for that destination

                                    response.uniqueIDs.add(internodeUDP.getNeighbourUniqueIDFromName(destinationName));

                                    // Get the corresponding arrival time for the port
                                    Timetable.TimetableEntry arrivalEntry = timetable.getEarliestTimetableEntryByDest(destinationName, request.departTime);
                                    response.arrivalTimes.add(arrivalEntry.arrivalTime);

                                    // If this port is the destination, set the appropriate flag
                                    if (destinationName.equals(request.destinationName)) {
                                        response.isDestIndex = (short) (response.uniqueIDs.size() - 1);
                                    }
                                } else {
                                    // Can get to, but has no server associated with it

                                    // Only if this destination is the intended destination, add and set the appropriate flag
                                    if (destinationName.equals(request.destinationName)) {
                                        response.uniqueIDs.add((short) 0); // No next server exists, but this information is not needed by shortest path, still need to add something though

                                        // Get the corresponding arrival time for the destination
                                        Timetable.TimetableEntry arrivalEntry = timetable.getEarliestTimetableEntryByDest(destinationName, request.departTime);
                                        response.arrivalTimes.add(arrivalEntry.arrivalTime);

                                        response.isDestIndex = (short) (response.uniqueIDs.size() - 1);
                                    }
                                }
                            }
                            response.currentPathIndex = (short) (response.path.size() - 2); // Start path back by going next to the second last in path

                            String nextPort = internodeUDP.getNeighbourPortFromUID(response.path.get(response.currentPathIndex));
                            internodeUDP.sendMessage(nextPort, InternodeUDP.InternodeType_NodeDataResponse, response.toData());

                        } else {
                            // Message meant for another, send on to next

                            request.currentPathIndex++; // Go to next in path

                            String nextPort = internodeUDP.getNeighbourPortFromUID(request.path.get(request.currentPathIndex));

                            internodeUDP.sendMessage(nextPort, InternodeUDP.InternodeType_NodeDataRequest, request.toData());
                        }

                        break;
                    }

                    case InternodeUDP.InternodeType_NodeDataResponse: {
                        System.out.println("PROCESSING Received 'NodeDataResponse' internode packet with " + msg.data.remaining() + " bytes");

                        ShortestPathTraversal.NodeDataResponse response = ShortestPathTraversal.NodeDataResponse.fromData(msg.data);

                        if (response.currentPathIndex == 0) {
                            // Message meant for this server, inform shortest path traversal, potentially send another request

                            if (traversalInstanceMap.containsKey(response.traversalID)) {
                                TraversalInstance traversalInstance = traversalInstanceMap.get(response.traversalID);

                                ShortestPathTraversal.NodeDataRequest request = traversalInstance.traversal.traverse(response);

                                if (request != null) {
                                    // Another request to send

                                    ByteBuffer request_data = request.toData();

                                    String requestPort = internodeUDP.getNeighbourPortFromUID(request.path.get(request.currentPathIndex));

                                    internodeUDP.sendMessage(requestPort, InternodeUDP.InternodeType_NodeDataRequest, request_data);
                                } else {
                                    // Done searching, response to browser

                                    String response_str;
                                    if (traversalInstance.traversal.result() != null) {

                                        short arrivalTime = traversalInstance.traversal.result().arrivalTime;
                                        short firstStopUID = traversalInstance.traversal.result().path.get(1);

                                        String firstStopName = internodeUDP.getNeighbourNameFromUID(firstStopUID);

                                        Timetable.TimetableEntry first_stop = traversalInstance.timetable.getEarliestTimetableEntryByDest(firstStopName, traversalInstance.traversal.getStartTime());

                                        response_str = Toolbox.formResponseGeneral(first_stop, traversalInstance.traversal.getStartTime(), arrivalTime,
                                                traversalInstance.traversal.getDestinationName(),
                                                traversalInstance.traversal.result().dayJumps);
                                        //TODO possibly give full path?
                                    } else {
                                        response_str = "You can not reach: " + traversalInstance.traversal.getDestinationName();
                                    }

                                    traversalInstance.responder.sendAndClose("200 OK", response_str);

                                    traversalInstanceMap.remove(response.traversalID);
                                }
                            } else {
                                System.err.println("IGNORING Got node data response for unknown traversal ID.");
                            }
                        } else {
                            // Message meant for another, send on to next

                            response.currentPathIndex--; // Go to previous in path

                            String nextPort = internodeUDP.getNeighbourPortFromUID(response.path.get(response.currentPathIndex));

                            internodeUDP.sendMessage(nextPort, InternodeUDP.InternodeType_NodeDataResponse, response.toData());
                        }

                        break;
                    }

                }

            }
        }
    }
}
