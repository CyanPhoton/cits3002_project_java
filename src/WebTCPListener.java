/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;

import static java.net.StandardSocketOptions.SO_REUSEADDR;
import static java.net.StandardSocketOptions.SO_REUSEPORT;

public class WebTCPListener {
    public static class HTTPRequest {
        public String header;

        public HTTPRequest(String header) {
            this.header = header;
        }

        public String extractGetURI() {
            String[] sections = header.split(" ", 3);

            // METHOD
            if (!sections[0].equals("GET")) {
                return "";
            }

            return sections[1];
        }

        public HashMap<String, String> extractGetKeyValuePairs(String uri) {
            if (!uri.contains("/?")) {
                return new HashMap<>();
            }

            String[] mainParts = uri.split("/\\?", 2);
            System.out.println("mainParts = " + Arrays.toString(mainParts));

            String[] pairs = mainParts[1].split("&");
            System.out.println("pairs = " + Arrays.toString(pairs));

            HashMap<String, String> result = new HashMap<>();
            for (String pair : pairs) {
                String[] keyValue = pair.split("=", 2);
                System.out.println("keyValue = " + Arrays.toString(keyValue));
                result.put(keyValue[0], keyValue[1]);
            }

            return result;
        }
    }

    public static class WebTCPResponder {
        SocketChannel remoteSocket;

        WebTCPResponder(SocketChannel remoteSocket) {
            this.remoteSocket = remoteSocket;
        }

        public void sendAndClose(String status, String body) throws IOException {
            if (remoteSocket == null) {
                throw new RuntimeException("Must not try to send a response after already closing");
            }

            String responseString = "HTTP/1.1 " + status + "\r\n" +
                    "Content-Length: " + body.length() + "\r\n" +
                    "Content-Type: text/html\r\n" +
                    "Connection: Closed\r\n" +
                    "\r\n" +
                    body;

            ByteBuffer outBuffer = ByteBuffer.allocate(responseString.length());
            outBuffer.put(responseString.getBytes());
            outBuffer.flip();

            while (outBuffer.remaining() != 0) {
                remoteSocket.write(outBuffer);
            }

            remoteSocket.close();
            remoteSocket = null;
        }
    }

    public static class WebResponse {
        public HTTPRequest httpRequest;
        public WebTCPResponder webTCPResponder;

        public WebResponse(HTTPRequest httpRequest, WebTCPResponder webTCPResponder) {
            this.httpRequest = httpRequest;
            this.webTCPResponder = webTCPResponder;
        }
    }

    private static final int BACK_LOG = 10;
    private static final int TEMP_BUFFER_SIZE = 4096;

    private final ServerSocketChannel socket;
    SelectionKey selectionKey;

    public WebTCPListener(String port) throws IOException {
        socket = ServerSocketChannel.open();
        socket.setOption(SO_REUSEADDR, true);
        socket.setOption(SO_REUSEPORT, true);
        socket.bind(new InetSocketAddress((InetAddress) null, Integer.parseInt(port)), BACK_LOG);
        socket.configureBlocking(false); // Needed for select
    }

    public void registerForSelect(Selector selector) throws ClosedChannelException {
        selectionKey = socket.register(selector, SelectionKey.OP_ACCEPT);
    }

    public WebResponse processSelect() throws IOException {
        if (!selectionKey.isAcceptable()) {
            return null;
        }

        SocketChannel remoteSocket = socket.accept();

        if (remoteSocket == null) {
            return null;
        }

        StringBuilder msgString = new StringBuilder();

        ByteBuffer tempBuffer = ByteBuffer.allocate(TEMP_BUFFER_SIZE);
        do {
            remoteSocket.read(tempBuffer);
            tempBuffer.flip();

            byte[] stringBuffer = new byte[tempBuffer.remaining()];
            tempBuffer.get(stringBuffer);

            msgString.append(new String(stringBuffer, StandardCharsets.UTF_8));
            tempBuffer.clear();

            // By HTTP spec the body starts after double \r\n https://www.w3.org/Protocols/rfc2616/rfc2616-sec5.html
            // and here we only care about the header, and only the header is sent since there is not body
        } while (msgString.length() < 4 || !msgString.substring(msgString.length() - 4).equals("\r\n\r\n"));

        return new WebResponse(
                new HTTPRequest(msgString.toString()),
                new WebTCPResponder(remoteSocket)
        );
    }
}
