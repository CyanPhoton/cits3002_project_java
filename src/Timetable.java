/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public class Timetable {
    public static class TimetableEntry {
        short departureTime; // Minutes past midnight
        String transportMode; // Bus number / Train line
        String location; //Bus stand / Train platform
        short arrivalTime; // Minutes past midnight
        String destinationName;

        public TimetableEntry(short departureTime, String transportMode, String location, short arrivalTime, String destinationName) {
            this.departureTime = departureTime;
            this.transportMode = transportMode;
            this.location = location;
            this.arrivalTime = arrivalTime;
            this.destinationName = destinationName;
        }
    }

    private static final String OPT_DIR = "timetables";

    // Key is destination name
    HashMap<String, ArrayList<TimetableEntry>> timetableEntries = new HashMap<>();

    public Timetable(String name) throws IOException {
        File timetableFile = new File("tt-" + name);
        if (!timetableFile.exists()) {
            timetableFile = new File(OPT_DIR + "/tt-" + name);

            if (!timetableFile.exists()) {
                throw new IllegalArgumentException("Failed to locate timetable file: tt-" + name);
            }
        }


        List<String> lines = Files.readAllLines(timetableFile.toPath());

        for (int i = 1; i < lines.size(); i++) { // Skip first line of file

            String line = lines.get(i);
            String[] sections = line.split(",");

            if (sections.length != 5) {
                throw new RuntimeException("Got line in timetable file without 5 sections: " + line);
            }

            TimetableEntry timetableEntry = new TimetableEntry(
                    Toolbox.clock24hTimeToInt(sections[0]),
                    sections[1],
                    sections[2],
                    Toolbox.clock24hTimeToInt(sections[3]),
                    sections[4]
            );

            if (!timetableEntries.containsKey(timetableEntry.destinationName)) {
                timetableEntries.put(timetableEntry.destinationName, new ArrayList<>());
            }
            timetableEntries.get(timetableEntry.destinationName).add(timetableEntry);
        }

        // Sort each list from earliest arrival to latest, so that next day loop around is simply [0]
        for (ArrayList<TimetableEntry> list : timetableEntries.values()) {
            list.sort(Comparator.comparingInt(o -> o.departureTime));
        }
    }

    static long getLastModified(String name) {
        File timetableFile = new File("tt-" + name);
        if (!timetableFile.exists()) {
            timetableFile = new File(OPT_DIR + "/tt-" + name);

            if (!timetableFile.exists()) {
                throw new IllegalArgumentException("Failed to locate timetable file: tt-" + name);
            } else {
                return timetableFile.lastModified();
            }
        } else {
            return timetableFile.lastModified();
        }
    }

    Set<String> getDestinations() {
        return timetableEntries.keySet();
    }

    ArrayList<TimetableEntry> getTimetableEntriesByDest(String destName) {
        return timetableEntries.get(destName);
    }

    TimetableEntry getEarliestTimetableEntryByDest(String destName, short fromTime) {
        if (!timetableEntries.containsKey(destName)) {
            return null;
        }

        TimetableEntry mostRecent = timetableEntries.get(destName).get(0);

        for (TimetableEntry entry : getTimetableEntriesByDest(destName)) {
            if (entry.departureTime >= fromTime && entry.arrivalTime < mostRecent.arrivalTime) {
                mostRecent = entry;
            }
        }

        return mostRecent;
    }

    public HashMap<String, ArrayList<TimetableEntry>> getTimetableEntries() {
        return timetableEntries;
    }
}
