/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

import java.nio.ByteOrder;
import java.time.LocalDateTime;

public class Toolbox {
    static short getNowTimeInt() {
        LocalDateTime now = LocalDateTime.now();

        return (short) (now.getMinute() + now.getHour() * 60);
    }

    static short clock24hTimeToInt(String clockTime) {
        String[] parts = clockTime.split(":");
        short result = Short.parseShort(parts[1]);
        result += Short.parseShort(parts[0]) * 60;

        return result;
    }

    static String intToClock12hTime(short intTime) {
        short minutes = (short) (intTime % 60);
        short hours = (short) ((intTime - minutes) / 60);

        boolean isPm = false;
        if (hours >= 12) {
            isPm = true;
            if (hours >= 13) {
                hours -= 12;
            }
        }

        return hours + (minutes < 10 ? ":0" : ":") + minutes + (isPm ? "pm" : "am");
    }

    static String formResponseImmediate(Timetable.TimetableEntry firstTimetable, short requestTime) {
        String result = "";

        if (requestTime > firstTimetable.departureTime) {
            // Departure time is next day

            result += "There is no path to " + firstTimetable.destinationName + " that leaves today, the first path tomorrow is:<br>";
        }

        result += "First leg: "
                + firstTimetable.transportMode
                + " from "
                + firstTimetable.location
                + " leaving at: "
                + intToClock12hTime(firstTimetable.departureTime)
                + " and arriving at "
                + intToClock12hTime(firstTimetable.arrivalTime);

        return result;
    }

    static String formResponseGeneral(Timetable.TimetableEntry firstTimetable, short requestTime, short arrivalTime, String destinationName, short dayJumps) {
        String result = "";

        if (requestTime > firstTimetable.departureTime) {
            // Departure time is next day

            result += "There is no path to " + destinationName + " that leaves today, the first path tomorrow is:<br>";
        } else if (dayJumps > 0) {
            // Leave today, but don't get there today

            result += "There is no path to " + destinationName + " that arrives today, the first path leaving today is:<br>";
        }

        result += "First leg: "
                + firstTimetable.transportMode
                + " from "
                + firstTimetable.location
                + " leaving at: "
                + intToClock12hTime(firstTimetable.departureTime)
                + (dayJumps > 0 ? (requestTime > firstTimetable.departureTime ? " Tomorrow" : " Today") : "") // Add specifying the depart day if needed
                + " and arriving to: "
                + destinationName
                + " at: "
                + intToClock12hTime(arrivalTime);

        if (dayJumps == 1) {
            result += " Tomorrow";
        } else if (dayJumps > 1) {
            result += " in " + dayJumps + " Days";
        }

        return result;
    }

    static short htons(short in) {
        if (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN) {
            return in;
        }
        return Short.reverseBytes(in);
    }
}
